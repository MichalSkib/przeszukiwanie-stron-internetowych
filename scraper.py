import requests
from bs4 import BeautifulSoup
import pprint


class Scraper:

    def __init__(self):
        self.URL = 'https://www.empik.com/bestsellery'
        self.page_source = self.get_page_source()
        self.books = {}

    def get_page_source(self):
        r = requests.get(self.URL)
        if r.status_code == 200:
            return r.text
        else:
            print("Błąd podczas pobierania treści strony...")

    def parse(self):
        soup = BeautifulSoup(self.page_source, 'html.parser')
        book_box = soup.find_all("div", {"class": "search-content"})[0]
        books = book_box.find_all("div", {"class": "search-list-item"})
        for book in books:
            title = book.find_all("strong", {"class": "ta-product-title"})[0]
            try:
                author = book.find_all("div", {"class": "smartAuthorWrapper"})[0]
            except IndexError:
                author = None
            else:
                author = author.text.replace("\n", "").replace(" ,", ", ")
            self.books[title.text.split("\n")[0]] = {
                "title": title.text.split("\n")[1],
                "author": author
            }
        pprint.pprint(self.books)


scraper = Scraper()
scraper.parse()
